// Copyright Epic Games, Inc. All Rights Reserved.

#include "Rodius_TDS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Rodius_TDS, "Rodius_TDS" );

DEFINE_LOG_CATEGORY(LogRodius_TDS)
 
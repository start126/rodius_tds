// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Rodius_TDSGameMode.generated.h"

UCLASS(minimalapi)
class ARodius_TDSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ARodius_TDSGameMode();
};




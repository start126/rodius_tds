// Copyright Epic Games, Inc. All Rights Reserved.

#include "Rodius_TDSGameMode.h"
#include "Rodius_TDSPlayerController.h"
#include "Rodius_TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARodius_TDSGameMode::ARodius_TDSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ARodius_TDSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
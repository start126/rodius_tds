// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Rodius_TDS : ModuleRules
{
	public Rodius_TDS(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" });
    }
}
